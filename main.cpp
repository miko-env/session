#include <QCoreApplication>
#include <QProcess>
#include <QStringList>
#include <QDebug>

const QList<QStringList> coreProcesses = {
    { "kwin_x11" },
    { "miko-settings", "--daemon" },
    { "miko-background-daemon" },
    { "miko-menu-bar" },
};

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QObject sharedParent;

    for (QStringList processArgs : coreProcesses) {
        QString program = processArgs[0];
        QStringList arguments;
        arguments << processArgs;
        arguments.removeFirst();
        qDebug() << "Starting" << program << arguments;
        QProcess* process = new QProcess(&sharedParent);
        process->start(program, arguments);
    }

    return app.exec();
}
